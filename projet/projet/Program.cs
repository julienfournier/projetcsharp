﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace projet
{
    class Program
    {
        static Random rand = new Random();
        static void Main(string[] args)
        {
            Pages.listPage();

            // Initilisation de la liste des contact (à faire ailleurs par la suite, voir dans un fichier ou bdd)
            Catcheur.initCatcheur();

            Menu.printMenu(1); // mettre 0 pour écran de sauvegarde (non fonctionel)
        }

        public static int Randomize(int min, int max)
        {
            int random = rand.Next(min, max);
            return random;
        }


    }
}
