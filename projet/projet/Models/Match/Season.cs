﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace projet
{
    class Season
    {
        public static int _week { get; set; } // numéro de semaine
        public static int _match { get; set; } // numéro du match
        public static float _bonus { get; set; }
    }
}
