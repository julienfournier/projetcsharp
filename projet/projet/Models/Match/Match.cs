﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace projet
{
    class Match : Season
    {
        public static List<Match> matchList = new List<Match>();

        public static List<Catcheur> oponent = new List<Catcheur>();

        public Catcheur _catcheur1 { get; set; }
        public Catcheur _catcheur2 { get; set; }
        public Catcheur _catcheurTurn { get; set; }
        public Catcheur _catcheurOponent { get; set; }
        public Catcheur _minLife { get; set; }
        public int _iteration { get; set; }
        public int _matchNum { get; set; } // numéro de match
        public bool _ko { get; set; } // est KO
        public Catcheur _winner { get; set; }
        public Catcheur _looser { get; set; }
        public int _season { get; set; }
        public int _gainMatch { get; set; }


        /// <summary>
        /// Génération d'un match. Selection de deux catcheurs disponibles au hasard dans la liste des catcheurs puis appel du lancement du match.
        /// </summary>
        public static void initMatch()
        {
            bool aleatoire = randomChoose();
            Catcheur oponent1 = null;
            Catcheur oponent2 = null;

            if (aleatoire == true)
            {
                int totalCatcheurAvailable = Catcheur.catcheurList.Where(catcheur => (catcheur._condition == conditionCatcheur.Disponible) && (catcheur._isPlaying == false)).Count();
                oponent1 = Catcheur.catcheurList.Where(catcheur => (catcheur._condition == conditionCatcheur.Disponible) && (catcheur._isPlaying == false)).ElementAt(Program.Randomize(0, totalCatcheurAvailable));
                oponent1._isPlaying = true;
                oponent2 = Catcheur.catcheurList.Where(catcheur => (catcheur._condition == conditionCatcheur.Disponible) && (catcheur._isPlaying == false)).ElementAt(Program.Randomize(0, totalCatcheurAvailable - 1));
                oponent1._isPlaying = false;
            }
            else
            {
                oponent1 = Match.selectCatcheur(1);
                oponent2 = Match.selectCatcheur(2);
            }

            Console.Clear();
            Console.WriteLine("Les dés sont jetés ! Les deux catcheurs qui vont s'affronter ce soir sont : ");
            Console.WriteLine($"Catcheur 1 : {oponent1.ToString()}");
            Console.WriteLine("Contre");
            Console.WriteLine($"Catcheur 2 : {oponent2.ToString()}");
            int matchId = matchList.Count();
            Console.WriteLine("Match ID : " + (matchId+1));
            Console.WriteLine("-------------");
            matchList.Add(new Match(oponent1, oponent2, matchId));
            round(matchId);

        }

        public static bool randomChoose()
        {
            int select = 0;
            ConsoleKey test;
            while (true)
            {
                Console.Clear();
                Console.WriteLine("Voulez vous choisir les catcheurs manuelement ou aléatoirement ?");

                if (select == 0)
                {
                    Console.BackgroundColor = ConsoleColor.Blue;
                    Console.WriteLine("Manuelement");
                    Console.ResetColor();
                    Console.WriteLine("Aléatoirement");
                }
                else
                {
                    Console.WriteLine("Manuelement");
                    Console.BackgroundColor = ConsoleColor.Blue;
                    Console.WriteLine("Aléatoirement");
                    Console.ResetColor();
                }
                test = Console.ReadKey().Key;
                if (test == ConsoleKey.DownArrow && select != 1)
                {
                    select++;
                }
                else if (test == ConsoleKey.UpArrow && select != 0)
                {
                    select--;
                }
                else if (test == ConsoleKey.Enter)
                {
                    if (select == 0)
                    {
                        return false;
                    }
                    else { return true; }
                }

            }
        }

        public static Catcheur selectCatcheur(int selected)
        {
            int count = 0;
            int catcheurNum = 0;
            ConsoleKey test = ConsoleKey.A;
            while (test != ConsoleKey.Enter)
            {
                Console.Clear();
                if (selected == 1)
                {
                    Console.WriteLine("Selectionnez votre premier catcheur");
                }
                else
                {
                    Console.WriteLine("Selectionnez votre second catcheur");
                }
                catcheurNum = 0;
                foreach (Catcheur catcheur in Catcheur.catcheurList.Where(c => ((c._condition == conditionCatcheur.Disponible) && (c._isPlaying == false))))
                {
                    if (count == catcheurNum)
                    {
                        Console.BackgroundColor = ConsoleColor.Blue;
                        Console.WriteLine($"{catcheur._name} (Entrée)");
                        Console.ResetColor();
                    }
                    else
                    {
                        Console.WriteLine($"{catcheur._name}");
                    }
                    catcheurNum++;
                }
                test = Console.ReadKey().Key;
                if (test == ConsoleKey.UpArrow && count > 0)
                {
                    count--;
                }
                else if (test == ConsoleKey.DownArrow && count < (catcheurNum - 1))
                {
                    count++;
                }
                else if (test == ConsoleKey.Enter)
                {
                    Catcheur temp = Catcheur.catcheurList.Where(c => ((c._condition == conditionCatcheur.Disponible) && (c._isPlaying == false))).ElementAt(count);
                    temp._isPlaying = true;

                    return temp;
                }

            }

            return Catcheur.catcheurList.ElementAt(count);
        }

        public Match(Catcheur Catcheur1, Catcheur Catcheur2, int MatchID)
        {
            _matchNum = MatchID;
            _gainMatch = 0;
            int temp = Program.Randomize(0, 2);
            if (temp == 1)
            {
                _catcheur1 = Catcheur1;
                _catcheur2 = Catcheur2;
            }
            else
            {
                _catcheur1 = Catcheur2;
                _catcheur2 = Catcheur1;
            }
        }

        public static void round(int matchID)
        {
            Match match = matchList.ElementAt(matchID);
            match._iteration = 1; // premier tour du round, compteur de tour à 1
            while ((match._iteration <= 20) && (match._catcheur1._hp > 0) && (match._catcheur2._hp > 0)) // round tant que les enemis ne sont pas mort, ou que le round ne dépasse pas 20 tours
            {

                Console.BackgroundColor = ConsoleColor.White;
                Console.ForegroundColor = ConsoleColor.Black;
                Console.WriteLine($"--- Tour {match._iteration} ---");
                Console.ResetColor();
                Console.WriteLine();

                // ici déroulement du tour
                match.turn(match._catcheur1, match._catcheur2);
                match.turn(match._catcheur2, match._catcheur1);
                Thread.Sleep(2000); //décomenté / commenté pour attente ou non
                match._iteration++; // fin d'un tour, ittération du compeurs de tours

                Console.BackgroundColor = ConsoleColor.White;
                Console.ForegroundColor = ConsoleColor.Black;
                Console.WriteLine($"--- --- --- ---");
                Console.ResetColor();
                Console.WriteLine();
            }
            match.completeMatch(matchID);
        }

        public void turn(Catcheur _catcheurTurn, Catcheur _catcheurOponent)
        {
            _catcheurTurn.defineAction();
            if (_catcheurTurn._action == actionCatcheur.Attaque)
            {
                Console.Write($"Le catcheur {_catcheurTurn._name} ");
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine($"attaque ");
                Console.ResetColor();
                Console.WriteLine($"{_catcheurOponent._name}");
                _catcheurTurn.play(_catcheurOponent);
            }
            else if (_catcheurTurn._action == actionCatcheur.Defendre)
            {
                Console.Write($"Le catcheur {_catcheurTurn._name} ");
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("défend");
                Console.ResetColor();
            }
            else if (_catcheurTurn._action == actionCatcheur.Competence)
            {
                Console.Write($"Le catcheur {_catcheurTurn._name} tente d'utiliser sa ");
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine("compétence spéciale");
                Console.ResetColor();
                _catcheurTurn.play(_catcheurOponent);
            }
            Console.WriteLine("");
        }

        public void completeMatch(int matchID)
        {
            Match match = matchList.ElementAt(matchID);
            match._looser = match._catcheur1;
            match._winner = match._catcheur2;

            // si pas de KO
            if ((match._catcheur1._hp > 0) && (match._catcheur2._hp > 0))
            {
                Console.WriteLine($"Le match se termine au bout de {match._iteration - 1} itérations, personne n'est KO");

                if (match._catcheur1._hp > match._catcheur2._hp)
                {
                    // catcheur 1 gagne
                    match._winner = _catcheur1;
                    match._looser = _catcheur2;
                    match._winner._victory++;

                }
                else if (match._catcheur1._hp < match._catcheur2._hp)
                {
                    // catcheur 2 gagne
                    match._winner = _catcheur2;
                    match._looser = _catcheur1;
                    match._winner._victory++;
                }
                else
                {
                    // égalitée
                    match._winner = null;
                }


                Console.WriteLine($"{match._looser._name} perd il lui reste {match._looser._hp} hp");
                if (match._looser._hp < (match._looser._maxHp / 2))
                {
                    Console.WriteLine($"Le catcheur {match._looser._name} perd, il a moins de {match._looser._maxHp / 2} HP et va donc à l'hopital pour se remettre en forme");
                    match._looser._condition = conditionCatcheur.Convalescence;
                    match._looser._downTime = Program.Randomize(2, 6);
                }
                match._ko = false;
            }
            else // si KO
            {
                match._ko = true;
                if (match._catcheur1._hp <= 0 && match._catcheur2._hp > 0) // si catcheur 1 KO
                {
                    match._looser = _catcheur1;
                    match._winner = _catcheur2;
                    match._winner._victory++;
                }
                else if (match._catcheur2._hp <= 0 && match._catcheur1._hp > 0) // si catcheur 2 KO
                {
                    match._looser = _catcheur2;
                    match._winner = _catcheur1;
                    match._winner._victory++;
                }
                else if (match._catcheur1._hp <= 0 && match._catcheur2._hp <= 0) // si double KO
                {
                    match._winner = null;
                    match._looser = null;
                    match._catcheur1._condition = conditionCatcheur.Mort;
                    match._catcheur2._condition = conditionCatcheur.Mort;
                    match._catcheur1._hp = 0;
                    match._catcheur2._hp = 0;
                }
                if (match._looser != null)
                {
                    match._looser._condition = conditionCatcheur.Mort;
                    match._looser._hp = 0;
                }
                if (match._winner != null)
                {
                    if (_winner._hp < (_winner._maxHp / 2))
                    {
                        Console.WriteLine($"Le catcheur {_winner._name} gagne, mais il a moins de {_winner._maxHp / 2} HP et va donc à l'hopital pour se remettre en forme");
                        _winner._condition = conditionCatcheur.Convalescence;
                        _winner._downTime = Program.Randomize(2, 6);
                    }
                    else
                    {
                        Console.WriteLine($"Le catcheur {_winner._name} met son adversaire KO, il regagne l'ensemble de sa vie.");
                        _winner._hp = _winner._maxHp;
                    }
                }
            }
            Game._money += moneyEarn();
            _matchNum++;
            Season._match++;
            if (Season._match % 8 == 0 )
            {
                Console.WriteLine($"8éme match de la saison {_week}");
                Console.WriteLine("Lancement de la déclaration au fisc");
                _week++;
                _bonus = (float)Math.Pow(1.13, (_week - 1));
                //_matchNum = 1;
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine($"Nouveau bonus : {_bonus}");
                Console.ResetColor();
                Season._match = _matchNum;
            }

            // disponibilité des catcheurs
            _catcheur1._isPlaying = false;
            _catcheur2._isPlaying = false;
            _catcheur1._matchPlayed++;
            _catcheur2._matchPlayed++;

            Console.WriteLine();
            Menu.pressToContinue();
            Console.ReadKey();
            reviewMatch(matchID);//recap du match
            checkHospital(); // vérification de l'hopital
            
        }

        /// <summary>
        /// Liste des matchs
        /// </summary>
        /// <param name="filter">Filtrer par KO / ittérations / all</param>
        public static void listMatch(string filter)
        {
            if (filter == "KO")
            {
                foreach (Match match in matchList.Where(match => match._ko == true))
                {
                    Console.WriteLine(match.ToString());
                    Console.WriteLine("");//saut de ligne
                }
            }
            else if (filter == "timeOut")
            {
                foreach (Match match in matchList.Where(match => match._ko == false))
                {
                    Console.WriteLine(match.ToString());
                    Console.WriteLine("");//saut de ligne
                }
            }
            else if (filter == "all")
            {
                foreach (Match match in matchList)
                {
                    Console.WriteLine(match.ToString());
                    Console.WriteLine("");//saut de ligne
                }
            }
        }

        public int moneyEarn()
        {
            int gainMatch = 5000 * _iteration;
            if (_ko)
            {
                gainMatch += 10000;
            }
            else
            {
                gainMatch += 1000;
            }
            if(Season._week != 1)
            {
                Console.WriteLine($"Votre gain de {gainMatch} est multiplié par votre bonus de {_bonus}");
                float gaintemp = (float)gainMatch * Season._bonus;
                gainMatch = (int)gaintemp;
            }
            return gainMatch;
        }

        public void reviewMatch(int matchID)
        {
            Match match = matchList.ElementAt(matchID);
            Console.Clear();
            if ((match._ko == true) && (match._winner != null))
            {
                Console.WriteLine($"Le match numéro {_matchNum} opposant {match._catcheur1._name} à {match._catcheur2._name}, s'est fini par un KO");
                Console.WriteLine($"{match._winner._name} a gagné en {match._iteration} tours.");
                Console.WriteLine($"{match._looser._name} est mort");
                if (match._winner._hp < (match._winner._maxHp / 2))
                {
                    Console.WriteLine($"{match._winner._name} n'est pas en forme. Il part en Convalescence");
                }
            }
            else if ((match._ko == false) && (match._winner != null))
            {
                Console.WriteLine($"Le match numéro {_matchNum} opposant {match._catcheur1._name} à {match._catcheur2._name}, a été remporté par {match._winner._name}");
                Console.WriteLine($"Il reste {match._looser._hp} hp à {match._looser._name}");
                if (match._looser._hp < (match._looser._maxHp / 2))
                {
                    Console.WriteLine($"{match._looser._name} n'est pas en forme. Il part en Convalescence");
                }
            }
            else
            {
                Console.WriteLine("pas de gagnant");
            }
            Console.WriteLine("");
            match._gainMatch = moneyEarn();
            Console.WriteLine($"Vous venez de gagner {match._gainMatch} euros avec ce match");
            Console.WriteLine();
            Menu.pressToContinue();
            Console.ReadKey();
        }

        public void checkHospital()
        {
            Console.Clear();
            Console.WriteLine("Vérification de l'hopital");
            Console.WriteLine("-------------------------");
            foreach (Catcheur catcheur in Catcheur.catcheurList.Where(c => c._condition == conditionCatcheur.Convalescence).OrderBy(c => c._downTime))
            {
                catcheur._downTime--;
                if (catcheur._downTime <= 0)
                {
                    catcheur._condition = conditionCatcheur.Disponible;
                    catcheur._hp = catcheur._maxHp;
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine($"{catcheur._name} est à nouveau disponible pour le combat !");
                    Console.ResetColor();
                }
                else
                {
                    Console.WriteLine($"{catcheur._name} est à l'hopital avec {catcheur._downTime} semaine(s) de convalescence");
                }
            }
            Console.WriteLine("");
            Menu.pressToContinue();
            Catcheur.gameOver();
        }

        public override string ToString()
        {
            if (this._ko == true)
            {
                if (this._catcheur1._hp <= 0 && this._catcheur2._hp <= 0)
                {
                    return $"Le match numéro {this._matchNum} opposant {this._catcheur1._name} à {this._catcheur2._name}, s'est fini par un double KO. \nIl n'y a donc pas de gagnant";

                }
                else
                {
                    return $"Le match numéro {this._matchNum} opposant {this._catcheur1._name} à {this._catcheur2._name}, s'est fini par un KO de {this._looser._name}, en {this._iteration} tours. \nLe gagnant est donc : {this._winner._name}";
                }
            }
            else
            {
                return $"Le match numéro {this._matchNum} oposant {this._catcheur1._name} à {this._catcheur2._name}, à été remporté par {this._winner._name}. \nIl avait {this._winner._hp} HP, son adversaire à fini avec {this._looser._hp} HP";
            }
        }
    }
}
