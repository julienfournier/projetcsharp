﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace projet
{
    class Game
    {
        public int _id { get; set; }
        public string _user { get; set; }
        public int _currentSeason { get; set; }
        public static int _money { get; set; }

        public Game(int Money)
        {
            _money = Money;
        }

        public void addUser ()
        {

        }

        public static void loadGame()
        {
            Console.WriteLine("Pas de partie sauvegarder");
            Game._money = 0;
            Season._week = 1;
        }

        public static void startNewGame()
        {
            Console.WriteLine("Vous venez de lancer une nouvelle partie");
            Game._money = 0;
            Season._week = 1;
        }
    }

}
