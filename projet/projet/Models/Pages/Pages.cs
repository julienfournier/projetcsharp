﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace projet
{
    class Pages
    {
        public static List<Pages> pagesList = new List<Pages>();

        public string _name { get; set; }
        public string _desc { get; set; }
        public int _number { get; set; }
        public bool _launchFunc { get; set; }
        public string _funcId { get; set; }

        public Pages(string Name, string Desc, int Number, string FuncId)
        {
            _name = Name;
            _desc = Desc;
            _number = Number;
            if (FuncId != "") // Si une fonction a éxécutée, on attribue l'id pour l'éxécuter dans le menu
            {
                _launchFunc = true;
                _funcId = FuncId;
            }
        }

        public static void listPage ()
        {
            // Pages
            Pages.pagesList.Add(new Pages("Lancement", "Continuer et commencer une partie ?", 0, ""));
            Pages.pagesList.Add(new Pages("Accueil", "Page d'accueil", 1, ""));
            Pages.pagesList.Add(new Pages("Liste des catcheurs", "Annuaire de catcheurs", 2, ""));
            Pages.pagesList.Add(new Pages("Match", "Lancement du match", 3, "initMatch()"));
            Pages.pagesList.Add(new Pages("Historique des matchs", "Voici l'historique des matchs", 4, ""));
            Pages.pagesList.Add(new Pages("Liste des contacts", "Voici la liste des contact", 5, "listCatcheur()"));
            Pages.pagesList.Add(new Pages("Rechercher un contact", "Saisissez une recherche", 6, "searchContact()"));
            Pages.pagesList.Add(new Pages("Liste des matchs par KO", "", 7, "listMatch(KO)"));
            Pages.pagesList.Add(new Pages("Liste des matchs par ittérations", "", 8, "listMatch(timeOut)"));
            Pages.pagesList.Add(new Pages("Liste des matchs, tout matchs confondus", "", 9, "listMatch(all)"));
            Pages.pagesList.Add(new Pages("Créer une nouvelle partie", "", 100, "startNewGame()"));
            Pages.pagesList.Add(new Pages("Charger une partie enregistrée", "", 101, "loadGame()"));

            // Menus de la page 0
            Menu.menuList.Add(new Menu("Démarrer une nouvelle partie", "", 0, 100));
            Menu.menuList.Add(new Menu("Continuer une partie sauvegardée", "", 0, 101));

            // Menus de la page 1 :
            Menu.menuList.Add(new Menu("Liste des catcheurs", "", 1, 2));
            Menu.menuList.Add(new Menu("Créer le prochain match", "", 1, 3));
            Menu.menuList.Add(new Menu("Consulter l'historique des matchs", "", 1, 4));

            // Menus de la page 2 :
            Menu.menuList.Add(new Menu("Lister les contacts", "", 2, 5));
            Menu.menuList.Add(new Menu("Rechercher un contact", "", 2, 6));

            // Menus de la page 3 :
            Menu.menuList.Add(new Menu("Afficher les matchs par KO", "", 4, 7));
            Menu.menuList.Add(new Menu("Afficher les matchs par ittérations", "", 4, 8));
            Menu.menuList.Add(new Menu("Afficher les deux", "", 4, 9));
        }
    }
}
