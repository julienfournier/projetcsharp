﻿using System;
using System.Collections.Generic;
using System.Linq;
using projet.Extra;
using System.Text;
using System.Threading.Tasks;

namespace projet
{
    class Menu : Pages
    {
        public static List<Menu> menuList = new List<Menu>();

        public new int _number { get; set; }
        public int _target { get; set; }
        public bool _active { get; set; }

        /// <summary>
        /// Constructeur de menu
        /// </summary>
        /// <param name="Name">Nom du menu : affiché dans les menus</param>
        /// <param name="Desc">Description du menu : pas encore utilisé</param>
        /// <param name="Number">Page sur laquelle le menu doit être présent</param>
        /// <param name="Target">Page cible du menu : Attention d'être sur qu'elle existe</param>
        public Menu(string Name, string Desc, int Number, int Target) : base(Name, Desc, Number, "")
        {
            _number = Number;
            _target = Target;
            _active = false;
        }

        /// <summary>
        /// Fonction de sortie du jeu
        /// </summary>
        /// <returns></returns>
        public static int exit()
        {
            int select = 0;
            ConsoleKey test;
            while (true)
            {
                Console.Clear();
                Console.WriteLine("Êtes vous sur de vouloir quiter ce super jeux ?");

                if (select == 0)
                {
                    Console.BackgroundColor = ConsoleColor.Blue;
                    Console.WriteLine("Non");
                    Console.ResetColor();
                    Console.WriteLine("Oui");
                }
                else
                {
                    Console.WriteLine("Non");
                    Console.BackgroundColor = ConsoleColor.Blue;
                    Console.WriteLine("Oui");
                    Console.ResetColor();
                }
                test = Console.ReadKey().Key;
                if (test == ConsoleKey.DownArrow && select != 1)
                {
                    select++;
                }
                else if (test == ConsoleKey.UpArrow && select != 0)
                {
                    select--;
                }
                else if (test == ConsoleKey.Enter)
                {
                    if (select == 0)
                    {
                        return 0;
                    }
                    else { return 1; }
                }

            }
        }

        public static void end()
        {
            Console.Clear();
            Console.WriteLine("GAME OVER !!!!!!!!");
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine($"Votre score est de {Game._money}");
            Console.Read();
            Environment.Exit(0); //quitter le jeu
        }

        /// <summary>
        /// Fonction d'affichage du menu - centre du jeu
        /// </summary>
        /// <param name="page"></param>
        public static void printMenu(int page)
        {
            int count = 0;
            int exit = 0;
            int prevpage = page;
            ConsoleKey test = ConsoleKey.A;
            menuList.First(menu => menu._number == page)._active = true;
            while (exit == 0)
            {
                Console.Clear();
                if (page == 1)
                {
                    Console.WriteLine("Bienvenue dans le gestionnaire de match");
                    if (Season._week == 1)
                    {
                        Console.WriteLine($"Solde disponible : {Game._money}          ||  Bonus disponible : 0 %");
                    }
                    else
                    {
                        Console.WriteLine($"Solde disponible : {Game._money}          ||  Bonus disponible : {(Season._bonus - 1) * 100} %");
                    }
                    Console.WriteLine($"Saison actuelle : {Season._week} - Match en cours : {Season._match}");
                    Console.WriteLine();
                }
                else
                {
                    Console.WriteLine($"{ pagesList.First(p => p._number == page)._name} ");
                    Console.WriteLine($"{ pagesList.First(p => p._number == page)._desc} ");
                    Console.WriteLine();
                }
                foreach (Menu menu in menuList.Where(menu => menu._number == page))
                {
                    if (menu._active == true)
                    {
                        Console.BackgroundColor = ConsoleColor.Blue;
                        Console.WriteLine($"{menu._name} (Enter)");
                        Console.ResetColor();
                    }
                    else
                    {
                        Console.WriteLine($"{menu._name}");
                    }
                }
                Console.WriteLine();
                Console.WriteLine("Press RETURN to go back");
                test = Console.ReadKey().Key;

                if (test == ConsoleKey.DownArrow && (count < (menuList.Where(menu => menu._number == page).Count() - 1)))
                {
                    menuList.FirstOrDefault(menu => (menu._active == true) && (menu._number == page))._active = false;
                    count++;
                    menuList.Where(menu => menu._number == page).ElementAt(count)._active = true;
                }
                if (test == ConsoleKey.UpArrow && (count > 0))
                {
                    menuList.FirstOrDefault(menu => (menu._active == true) && (menu._number == page))._active = false;
                    count--;
                    menuList.Where(menu => menu._number == page).ElementAt(count)._active = true;
                }
                if(test == ConsoleKey.Spacebar)
                {
                    Songs.cucaracha();
                }
                if (test == ConsoleKey.Backspace)
                {
                    if (page != 1)
                    {
                        menuList.First(menu => (menu._number == page) && (menu._active == true))._active = false;
                        page = prevpage;
                        menuList.First(menu => menu._number == page)._active = true;
                        count = 0;
                    }
                    else
                    {
                        exit = Menu.exit();
                    }
                }
                else if (test == ConsoleKey.Enter)
                {
                    // Vérification page
                    Console.Clear();
                    prevpage = page;
                    page = menuList.First(menu => menu._number == page && menu._active == true)._target;
                    if (pagesList.Any(p => p._number == page && p._launchFunc))
                    {
                        Pages current = pagesList.Single(p => p._number == page);
                        switch (current._funcId)
                        {
                            case "initMatch()":
                                Console.WriteLine("Lancement d'un match");
                                Console.WriteLine("--------------------");
                                Match.initMatch();
                                break;
                            case "listCatcheur()":
                                Console.WriteLine("Liste des contacts");
                                Console.WriteLine("------------------");
                                Catcheur.listCatcheur();
                                Console.WriteLine();
                                Menu.pressToContinue();
                                break;
                            case "searchContact()":
                                Console.WriteLine("Recherche d'un contact");
                                Console.WriteLine("----------------------");
                                Catcheur.searchCatcheur();
                                Console.WriteLine();
                                Menu.pressToContinue();
                                break;
                            case "listMatch(KO)":
                                Console.WriteLine("Liste des matchs par KO");
                                Console.WriteLine("-----------------------");
                                Match.listMatch("KO");
                                Console.WriteLine();
                                Menu.pressToContinue();
                                break;
                            case "listMatch(timeOut)":
                                Console.WriteLine("Liste des matchs par itération");
                                Console.WriteLine("------------------------------");
                                Match.listMatch("timeOut");
                                Console.WriteLine();
                                Menu.pressToContinue();
                                break;
                            case "listMatch(all)":
                                Console.WriteLine("Liste de tout les matchs");
                                Console.WriteLine("------------------------");
                                Match.listMatch("all");
                                Console.WriteLine();
                                Menu.pressToContinue();
                                break;
                            case "startNewGame()":
                                Game.startNewGame();
                                break;
                            case "loadGame()":
                                Game.loadGame();
                                break;
                            default:
                                break;
                        }
                        Console.ReadKey();

                        menuList.First(menu => menu._number == prevpage && menu._active)._active = false;
                        page = 1;
                        menuList.First(menu => menu._number == page)._active = true;
                        count = 0;
                    }
                    else
                    {
                        menuList.First(menu => menu._number == page)._active = true;
                        count = 0;
                        menuList.FirstOrDefault(menu => (menu._active == true) && (menu._number == prevpage))._active = false;
                    }
                }
            }
        }

        public static void pressToContinue()
        {
            Console.BackgroundColor = ConsoleColor.White;
            Console.WriteLine("Pressez une touche pour continuer");
            Console.ResetColor();
        }
    }
}
