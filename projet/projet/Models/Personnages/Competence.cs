﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace projet
{
    abstract class Competence : Catcheur
    {
        public Competence(string Name, typeCatcheur Type, conditionCatcheur Condition, actionCatcheur Action) : base(Type, Condition, Name)
        {
        }

        public static void AddHp(int val,Catcheur target)
        {
            if (target._hp + val <= target._maxHp)
            {
                target._hp += val;
                Console.WriteLine($"{target._name} gagne {val} point de vie");
            }
        }

        public static void AddDamage(int val,Catcheur player, Catcheur enemy)
        {
            player._attack += val;
            Console.WriteLine($"Attaque de {player._name} est passé à {player._attack}");
            player._action = actionCatcheur.Attaque;
            player.play(enemy);
            player._attack -= val;
        }

        public static void LoseHp(int val, Catcheur target)
        {
            target._hp -= val;
            Console.WriteLine($"Le joueur {target._name} perd {val} point de vie");
        }


        public static void KO(Catcheur target)
        {
            target._hp = 0;
            Console.WriteLine($"{target._name} est KO");
        }

        public static void CancelAttack(Catcheur target)
        {
            target._canAttack = false;
            Console.WriteLine($"{target._name} ne peut plus attaquer !");
        }

        public static void BlockDamage(int val ,Catcheur player, Catcheur enemy)
        {
            player._action = actionCatcheur.Defendre;
            player._bonusDefense += val;
            player.play(enemy);
        }

        public static void BruteDammage(int val, Catcheur player, Catcheur enemy)
        {
            enemy._hp -= val;
            player._action = actionCatcheur.Attaque;
            player.play(enemy);

        }

        //---------------------------------------------------------------------------------------------

         public static void Comp(Catcheur player,Catcheur enemy)
        {
            switch (player._name)
            {
                case "L'ordonnateur des pompes funèbres":
                    if (Program.Randomize(0, 101) <= 30)
                    {
                        CancelAttack(enemy);
                    }
                    else
                    {
                        Console.WriteLine($"{player._name} rate sa compétence spéciale");
                    }
                    break;
                case "Juddy Sunny":
                    if (Program.Randomize(0, 101) <= 40)
                    {
                        AddHp(5,player);
                    }

                    if (Program.Randomize(0, 101) <= 60)
                    {
                        BlockDamage(1,player,enemy);
                    }
                    else
                    {
                        Console.WriteLine($"{player._name} rate sa compétence spéciale");
                    }
                    break;
                case "Triple Hache":
                    if (Program.Randomize(0, 101) <= 20)
                    {
                        AddDamage(2,player, enemy);
                        LoseHp(1, player);
                    }
                    else
                    {
                        Console.WriteLine($"{player._name} rate sa compétence spéciale");
                    }
                    break;
                case "Dead Poule":
                    if (Program.Randomize(0, 101) <=10)
                    {
                        BruteDammage(3,player, enemy);
                    }
                    if (Program.Randomize(0, 101) <=30)
                    {
                        AddHp(2, player);
                    }
                    if (Program.Randomize(0, 101) <=10)
                    {
                        BlockDamage(1,player, enemy);
                    }
                    else
                    {
                        Console.WriteLine($"{player._name} rate sa compétence spéciale");
                    }
                    break;
                case "Jarvan cinquième du nom":
                    if (Program.Randomize(0, 101) <= 30)
                    {
                        CancelAttack(enemy);
                    }
                    else
                    {
                        Console.WriteLine($"{player._name} rate sa compétence spéciale");
                    }
                    break;
                case "Madusa":
                    if (Program.Randomize(0, 101) <= 40)
                    {
                        BlockDamage(4,player,enemy);
                        LoseHp(1, enemy);
                    }
                    break;
                case "John Cinéma":
                    if (Program.Randomize(0, 101) <= 20)
                    {
                        AddDamage(2,player, enemy);
                        LoseHp(1,player);
                    }
                    else
                    {
                        Console.WriteLine($"{player._name} rate sa compétence spéciale");
                    }
                    break;
                case "Jeff Radis":
                    if (Program.Randomize(0, 101) <= 30)
                    {
                        CancelAttack(enemy);
                    }
                    else
                    {
                        Console.WriteLine($"{player._name} rate sa compétence spéciale");
                    }
                    break;
                case "Raie Mystérieuse":
                    if (Program.Randomize(0, 101) <= 40)
                    {
                        LoseHp(3, player);
                    }
                    else
                    {
                        AddDamage(1,player, enemy);
                        BlockDamage(2,player, enemy);
                    }
                    break;
                case "Chris Hart":
                    if (Program.Randomize(0, 101) <= 30)
                    {
                        CancelAttack(enemy);
                    }
                    else
                    {
                        Console.WriteLine($"{player._name} rate sa compétence spéciale");
                    }
                    break;
                case "Bret Benoit":
                    if (Program.Randomize(0, 101) <= 8)
                    {
                        KO(enemy);
                    }
                    else
                    {
                        Console.WriteLine($"{player._name} rate sa compétence spéciale");
                    }
                    break;
                default:
                    Console.WriteLine("--------Pas de compétence trouvée pour ce catcheur !--------");
                    break;
            }
        }
    }
}
