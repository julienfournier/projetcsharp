﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace projet
{

    enum typeCatcheur
    {
        Agile,
        Brute
    }

    enum conditionCatcheur
    {
        Disponible,
        Convalescence,
        Mort
    }

    enum actionCatcheur
    {
        Attaque,
        Defendre,
        Competence
    }

    class Catcheur
    {
        // Liste des catcheurs

        public static List<Catcheur> catcheurList = new List<Catcheur>();

        // Propriétés des catcheurs

        public string _name { get; set; }
        public typeCatcheur _type { get; set; }
        public string _techSpe { get; set; }
        public conditionCatcheur _condition { get; set; }
        public int _downTime { get; set; } // temps d'indisponibilité (généré auto entre 2 et 5 si en convalescence (defaite d'un match)
        public int _hp { get; set; }
        public int _attack { get; set; }
        public int _defense { get; set; }
        public int _bonusDefense { get; set; }
        public actionCatcheur _action { get; set; }
        public int _maxHp { get; set; }
        public bool _canAttack { get; set; }
        public bool _isPlaying { get; set; }
        public int _matchPlayed { get; set; }
        public int _victory { get; set; }


        /// <summary>
        /// Catcheurs
        /// </summary>
        /// <param name="Name">Nom du catcheur</param>
        /// <param name="Type">Type du catcheur : Agile, Brute</param>
        /// <param name="Tech">Technique spéciale du catcheur, à définir</param>
        /// <param name="Condition">Condition physique du catcheur : Disponible, En Convalescence, Mort</param>
        /// <param name="Action">Verifier si utile dans constructeur, j'en doute</param>
        /// 
        public Catcheur(typeCatcheur Type, conditionCatcheur Condition, string Name)
        {
            _name = Name;
            _type = Type;
            _condition = Condition;
            _action = actionCatcheur.Defendre;
            _bonusDefense = 0;

            if (Type == typeCatcheur.Agile)
            {
                _hp = 125; // 125
                _attack = 3;
                _defense = 3;
                _maxHp = _hp;
            }
            else if (Type == typeCatcheur.Brute)
            {
                _hp = 100; // 100
                _attack = 5;
                _defense = 1;
                _maxHp = _hp;
            }

            if (Condition == conditionCatcheur.Convalescence) // Si le catcheur est en covalescence, génération de la période d'indisponibilité
            {
                _downTime = Program.Randomize(2, 6);
            }
        }

        /// <summary>
        /// Fonction d'action d'un catcheur, pour un tour, dans une itération
        /// </summary>
        /// <param name="ennemy">Catcheur ennemi, peut être pas nécéssaire, à définir</param>
        /// 
        public void play(Catcheur ennemy)
        {
            this._bonusDefense = 0;
            if (this._action == actionCatcheur.Attaque)
            {
                if (_canAttack == true)
                {
                    if (ennemy._action == actionCatcheur.Defendre)
                    {
                        int degatDonne = (this._attack - ennemy._defense);
                        ennemy._hp = ennemy._hp - degatDonne;
                        Console.WriteLine($"Le catcheur {ennemy._name} se défend, les dégats infligés sont de {degatDonne}, la vie restante est de : {ennemy._hp}");
                    }
                    else
                    {
                        ennemy._hp -= this._attack;
                        Console.WriteLine($"Le catcheur {ennemy._name} est attaqué, les dégats infligés sont de {this._attack}, la vie restante est de : {ennemy._hp}");
                    }
                }
                else if (_canAttack == false)
                {
                    Console.WriteLine($"{this._name} essaie d'attaquer {ennemy._name}, mais il ne peut pas !");
                    this._canAttack = true;
                }
            }
            else if (this._action == actionCatcheur.Competence)
            {
                Competence.Comp(this, ennemy);
            }
            Console.WriteLine("");
        }

        public void defineAction()
        {
            int random = Program.Randomize(0, 3);
            switch (random)
            {
                case 0:
                    this._action = actionCatcheur.Attaque;
                    break;
                case 1:
                    this._action = actionCatcheur.Defendre;
                    break;
                case 2:
                    this._action = actionCatcheur.Competence;
                    break;
            }
        }

        public static void initCatcheur()
        {
            Catcheur.catcheurList.Add(new Catcheur(typeCatcheur.Brute, conditionCatcheur.Disponible, "L'ordonnateur des pompes funèbres"));
            Catcheur.catcheurList.Add(new Catcheur(typeCatcheur.Brute, conditionCatcheur.Disponible, "Juddy Sunny"));
            Catcheur.catcheurList.Add(new Catcheur(typeCatcheur.Agile, conditionCatcheur.Disponible, "Triple Hache"));
            Catcheur.catcheurList.Add(new Catcheur(typeCatcheur.Agile, conditionCatcheur.Disponible, "Dead Poule"));
            Catcheur.catcheurList.Add(new Catcheur(typeCatcheur.Brute, conditionCatcheur.Convalescence, "Jarvan cinquième du nom"));
            Catcheur.catcheurList.Add(new Catcheur(typeCatcheur.Agile, conditionCatcheur.Disponible, "Madusa"));
            Catcheur.catcheurList.Add(new Catcheur(typeCatcheur.Agile, conditionCatcheur.Convalescence, "John Cinéma"));
            Catcheur.catcheurList.Add(new Catcheur(typeCatcheur.Brute, conditionCatcheur.Convalescence, "Jeff Radis"));
            Catcheur.catcheurList.Add(new Catcheur(typeCatcheur.Brute, conditionCatcheur.Disponible, "Raie Mystérieuse"));
            Catcheur.catcheurList.Add(new Catcheur(typeCatcheur.Brute, conditionCatcheur.Disponible, "Chris Hart"));
            Catcheur.catcheurList.Add(new Catcheur(typeCatcheur.Agile, conditionCatcheur.Disponible, "Bret Benoit"));
        }

        public static void listCatcheur()
        {
            Console.WriteLine();
            Console.WriteLine("Catcheurs Disponibles : ");
            Console.WriteLine();
            foreach (Catcheur catcheur in catcheurList.Where(catcheur => catcheur._condition == conditionCatcheur.Disponible).OrderBy(catcheur => catcheur._name))
            {
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine(catcheur.ToString());
                Console.ResetColor();
            }
            Console.WriteLine();
            Console.WriteLine("Catcheurs en Convalescence : ");
            Console.WriteLine();
            foreach (Catcheur catcheur in catcheurList.Where(catcheur => catcheur._condition == conditionCatcheur.Convalescence).OrderBy(catcheur => catcheur._name))
            {
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine(catcheur.ToString());
                Console.ResetColor();
            }
            Console.WriteLine();
            Console.WriteLine("Catcheurs 6 pieds sous terre : ");
            Console.WriteLine();
            foreach (Catcheur catcheur in catcheurList.Where(catcheur => catcheur._condition == conditionCatcheur.Mort).OrderBy(catcheur => catcheur._name))
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(catcheur.ToString());
                Console.ResetColor();
            }
        }

        public static void searchCatcheur()
        {
            Console.WriteLine("Saisissez votre recherche :");
            string search = Console.ReadLine();

            Console.WriteLine($"Recherche de : {search}");

            if (!catcheurList.Any(c => c._name.ToLower().Contains(search.ToLower())))
            {
                Console.WriteLine("Pas de résultats pour la recherche : " + search);
            }
            else
            {
                foreach (Catcheur catcheur in catcheurList.Where(c => c._name.ToLower().Contains(search.ToLower())).OrderBy(c => c._condition))
                {
                    if (catcheur._condition == conditionCatcheur.Disponible)
                    {
                        Console.ForegroundColor = ConsoleColor.Green;
                        Console.WriteLine($"{catcheur._name} :  Disponible, il lui reste {catcheur._hp} HP");
                        Console.ResetColor();
                    }
                    else if (catcheur._condition == conditionCatcheur.Convalescence)
                    {
                        Console.ForegroundColor = ConsoleColor.Yellow;
                        Console.WriteLine($"{catcheur._name} :  En convalescence, il lui reste {catcheur._downTime} semaines d'absences");
                        Console.ResetColor();
                    }
                    else if (catcheur._condition == conditionCatcheur.Mort)
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine($"{catcheur._name} :  Décédé ...");
                        Console.ResetColor();
                    }

                }
            }
        }

        public static void gameOver()
        {
            if(catcheurList.Where(catcheur => catcheur._condition == conditionCatcheur.Disponible).Count()<2)
            {
                Menu.end();
            }
        }

        public override string ToString()
        {
            if (this._condition == conditionCatcheur.Disponible)
            {
                return $"Le catcheur {this._name} est de type {this._type}, il a {this._hp} hp, il est {this._condition} . ({this._victory}/{this._matchPlayed} victoires)";
            }
            else if(this._condition == conditionCatcheur.Convalescence)
            {
                return $"Le catcheur {this._name} est de type {this._type}, il a {this._hp} hp, il en {this._condition} pour encore {this._downTime} semaines. ({this._victory}/{this._matchPlayed} victoires)";
            }
            else
            {
                return $"Le catcheur {this._name} était de type {this._type}, il est malheureusement {this._condition} pour toujours ... ({this._victory}/{this._matchPlayed} victoires)";
            }
        }
    }
}
